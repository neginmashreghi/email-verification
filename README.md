### Description
Node.js application that implement Email Verification feature using Express, Nodemailer.

---
### Installtion

To get the Node server running locally:

* Clone this repo
* ```npm install``` to install all required dependencies
* ```npm start``` to start the server 

---
### Dependencies
* [express](https://expressjs.com/) - The server for handling and routing HTTP requests
* [Nodemaile](https://nodemailer.com/about/)- A module with zero dependencies for Node.js, designed for sending emails.

---

### Application Structure
* ```app.js``` - This file defines the Express server and routers to sends email and verifies pin verification
* ```emailForm.html```- This file contains an HTML form that takes an email address and calls ```/send``` endpoint to send an email message with a unique pin.
* ```pinFrom.html```- This file contains HTML form that takes verification pin and calls ```/verify``` endpoint to check if the pin is valid or not. 
